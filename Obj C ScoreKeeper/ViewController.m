//
//  ViewController.m
//  Obj C ScoreKeeper
//
//  Created by Francisco Morales on 9/22/17.
//  Copyright © 2017 Francisco Morales. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UITextField *team1TxtBox;
@property (strong, nonatomic) IBOutlet UITextField *team2TxtBox;


@property (strong, nonatomic) IBOutlet UILabel *team1Lbl;
@property (strong, nonatomic) IBOutlet UILabel *team2Lbl;

@property (strong, nonatomic) IBOutlet UIStepper *team1Stepper;
@property (strong, nonatomic) IBOutlet UIStepper *team2Stepper;

@property (weak, nonatomic) IBOutlet UITextField *team1TxtField;
@property (weak, nonatomic) IBOutlet UITextField *team2TxtField;


@end

@implementation ViewController

@synthesize team1TxtBox;
@synthesize team2TxtBox;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    team1TxtBox.delegate = self;
    team2TxtBox.delegate = self;
}


//HIDE KEYBOARD
//When user presses "Return"
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [team1TxtBox resignFirstResponder];
    [team2TxtBox resignFirstResponder];
    return true;
}
//When user touches outside of keyboard
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//STEPPER ACTION
- (IBAction)team1Stepper:(UIStepper *)sender {
    NSLog(@"%.0f", _team1Stepper.value);
    _team1Lbl.text = [NSString stringWithFormat:@"%.0f", _team1Stepper.value];
}
- (IBAction)team2Stepper:(UIStepper *)sender {
    NSLog(@"%.0f", _team2Stepper.value);
    _team2Lbl.text = [NSString stringWithFormat:@"%.0f", _team2Stepper.value];
}
//RESET BUTTON
- (IBAction)resetBtn:(UIButton *)sender {
    _team1Stepper.value = 0;
    _team1Lbl.text = @"0";
    _team2Stepper.value = 0;
    _team2Lbl.text = @"0";
}


@end
